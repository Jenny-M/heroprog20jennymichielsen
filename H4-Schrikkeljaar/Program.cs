﻿using System;

namespace H4_Schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            int year = Convert.ToInt32(Console.ReadLine());

            if (year % 400 == 0) {
                Console.WriteLine("schrikkeljaar");
            }
            else if (year % 100 == 0) {
                Console.WriteLine("geen schrikkeljaar");
            }
            else if (year % 4 == 0) {
                Console.WriteLine("schrikkeljaar");
            }
            else {
                    Console.WriteLine("geen schrikkeljaar");
            }
        }
    }
}
