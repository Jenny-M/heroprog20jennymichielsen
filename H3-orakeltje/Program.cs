﻿using System;

namespace H3_orakeltje
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randomGen = new Random();
            int number = randomGen.Next(5, 126);
            Console.WriteLine($"Je zal nog {number} jaar leven.");
        }
    }
}
