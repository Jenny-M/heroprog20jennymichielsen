﻿using System;

namespace H2_weerstandberekenaar_deel1
{
    class Program
    {
        static void Main(string[] args)
        {
            string circle1, circle2;
            int circle12;
            Double circle3;

            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            circle1 = Console.ReadLine();
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            circle2 = Console.ReadLine();
            Console.Write("Geef de waarde (uitgedrukt in een getal van -2 tot 7) van de derde ring(exponent): ");
            circle3 = int.Parse(Console.ReadLine());
            circle3 = Math.Pow(10, circle3);
            circle12 = int.Parse(circle1 + circle2);
            Console.WriteLine($"Resultaat is {circle12 * circle3} Ohm, ofwel {circle12}x{ circle3}.");
            Console.ReadLine();
        }
    }
}
