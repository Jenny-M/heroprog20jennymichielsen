﻿using System;

namespace H5OefeningenDeel1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H5_opwarmers_9");
            int i = -100;
            while (i <= 100)
            {
                Console.WriteLine(i++);

            }
            Console.ReadLine();
        
            int j = -100;
            Console.WriteLine("H5_opwarmers_11");
            for (j = -100; j <= 100; j++)
            {
                Console.WriteLine(j);
            }
            Console.ReadLine();
        
            int k = 1;
            Console.WriteLine("H5_opwarmers_13");
            for (k = 1; k <= 100; k++)
            {
                if ((k % 6) == 0 || (k % 8) == 0)
                {
                    Console.WriteLine(k);
                }
            }
                Console.ReadLine();
            

            int sum = 0;
                Console.WriteLine("H5_Euler_project");
                for (int m = 0; m <= 1000; m++)
                {
                    if (m % 3 == 0 || m % 5 == 0)
                    {
                        sum += m;
                    }
                    Console.WriteLine($"Sum:{sum}");
                }
                Console.ReadLine();

            int rows = 4;
            Console.WriteLine("H5-for_doordenker");
            for (int n = 1; n <= rows; ++n)
            {
                for (int o = 1; o <= n; ++o)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for (int n = 1; n <= rows - 1; ++n)
            {
                for (int o = 1; o <= 4 - n % 4; ++o)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            Console.ReadLine();

            int rrows = 6;
            int p = 0;
            int space = 0;
            Console.WriteLine("H5-for_doordenker_extra");
            for (int q = 1; q <= rrows; ++q, p = 0)
            {
                for (space = 1; space <= rrows - q; ++space)
                {
                    Console.Write(" ");
                }
                while (p != 1 * q - 1)
                {
                    Console.Write("* ");
                    ++p;
                }
                Console.WriteLine();
            }
            Console.ReadLine();
            }
        }
    }
