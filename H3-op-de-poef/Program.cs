﻿using System;

namespace H3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer bedrag in?");
            int sum = 0;
            sum += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("****************************");
            Console.WriteLine($"Het totaal van de poef is {sum} euro.");
            Console.WriteLine($"Dit zal {Math.Ceiling(sum / 10.0)} afbetalingen vragen.");
        }
    }
}
