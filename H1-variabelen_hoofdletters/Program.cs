﻿using System;

namespace H1_variabelen_hoofdletters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer tekst in met spaties, tekst van meer dan 100 karakters, zeker 1 karakter of voer helemaal geen tekst in ");
            String invoer = Console.ReadLine();
            invoer = invoer.ToUpper();
            Console.WriteLine("Uw invoer in hoofdletters: " + invoer);
            Console.ReadLine();
        }
    }
}
