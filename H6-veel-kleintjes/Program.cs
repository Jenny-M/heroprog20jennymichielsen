﻿using System;

namespace H6_veel_kleintjes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H6_veel_kleintjes");

            Console.WriteLine("Welke twee getallen wil je vergelijken?");


            int input1 = parseInt(prompt("Voer het eerste getal in: "));
            int input2 = parseInt(prompt("Voer het tweede getal in: "));

            if (input1 > input2)
            {
                Console.WriteLine(input1 + " is larger than " + input2);
            }
            else
            {
                Console.WriteLine(input1 + " is lesser than " + input2);
            }

         static bool ShowEven(int value)
            {
                return value % 2 != 0;
            }

            static void Main(string[] args)
            {
                int input = 2;

                Console.Write("ShowEven\n*******\n\nGeef de range in: ");
                input = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\nDe even getallen zijn:");

                for (int i = 0; i <= input; i++)
                {
                    if (ShowEven(i))
                    {
                        Console.Write($"{i} ");
                    }
                }
                Console.ReadLine();

            }
    }
}
