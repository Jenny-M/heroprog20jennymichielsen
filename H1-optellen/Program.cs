﻿using System;

namespace H1_optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal1 = 0;
            int getal2 = 0;
            int som = 0;
            string invoer = string.Empty;
            Console.WriteLine("Geef een getal: ");
            invoer = Console.ReadLine();
            getal1 = int.Parse(invoer);
            Console.WriteLine("Geef een tweede getal: ");
            invoer = Console.ReadLine();
            getal2 = int.Parse(invoer);
            som = getal1 + getal2;
            Console.WriteLine($"De som van de gegeven getallen is: {som}");
            Console.ReadLine();
        }
    }
}
