﻿using System;

namespace H2_systeem_informatie_pro
{
    class Program
    {
        static void Main(string[] args)
        {
            long cDriveInBytes = DriveInfo.GetDrives()[0].AvailableFreeSpace;
            long totalSize = DriveInfo.GetDrives()[0].TotalSize;
            Console.WriteLine($"Vrije ruimte op jouw c-schijf: {cDriveInBytes}");
            Console.WriteLine($"Totale ruimte van jouw c-schijf: {totalSize}");
            Console.ReadLine();
            Console.Write("\n\n********************************************* ***********************************\nGeef met nummer 1 t/m ... aan over welke harde schijf van jouw pc je info wenst: ");
            int input = Convert.ToInt32(Console.ReadLine()) - 1;
            long freeSize = DriveInfo.GetDrives()[input].AvailableFreeSpace;
            Console.WriteLine($"De vrije ruimte van {DriveInfo.GetDrives()[input].Name}
            is { freeSize / 100000000 } Gb);

			Console.ReadLine();
		}
    }
}
