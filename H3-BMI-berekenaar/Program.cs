﻿using System;

namespace H3_BMI_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double mass = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Je BMI bedraagt {mass / (height * height):F2}.");
        }
    }
}
