﻿using System;

namespace H0_eerste_programma_pro
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dit is mijn eerste C#-programma");
            Console.WriteLine("*******************************");
            Console.Write("Wat is je voornaam? ");
            string firstName = Console.ReadLine();
            Console.Write("En wat is je achternaam: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Dus je naam is: " + lastName + " " + firstName);
            Console.WriteLine("Of: " + firstName + " " + lastName);
        }
    }
}
