﻿using System;

namespace H5OefeningenDeel2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Lussen, Oefeningen Deel 2");
            Console.WriteLine("H5_oefeningen_1");

            int a = -100;
            while (a <= 100)
            {
                Console.WriteLine(a++);

            }
            Console.ReadLine();

            Console.WriteLine("H5_oefeningen_2");
            int b = -100;
            while (b <= 100)
            {
                Console.WriteLine(b++);

            }
            Console.ReadLine();

            Console.WriteLine("H5_oefeningen_3");
            int c = 1;
            while (c <= 100)
            {
                if ((c % 2) == 0)
                {
                    Console.WriteLine(c++);

                }
                Console.ReadLine();

            }
        }
    }
}