﻿using System;

namespace H0_rommelzin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat is je favoriete kleur?");
            string favoriteColor = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete eten?");
            string favoriteFood = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete auto?");
            string favoriteCar = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete film?");
            string favoriteMovie = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete boek?");
            string favoriteBook = Console.ReadLine();
            Console.WriteLine("Je favoriete kleur is " + favoriteFood + ". Je eet graag " + favoriteColor + favoriteCar + ". Je lievelingsfilm is " + favoriteBook + " en je favoriete boek is " + favoriteMovie + ".");
        }
    }
}
