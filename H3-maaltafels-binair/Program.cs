﻿using System;

namespace H3_maaltafels_binair
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ranGen = new Random();
            int multiplier;
            int randomNumber;

            multiplier = 1;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 2;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 3;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 4;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 5;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 6;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 7;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 8;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 9;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();

            multiplier = 10;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear(); Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2) }");
            Console.ReadLine();
        }
    }
}
