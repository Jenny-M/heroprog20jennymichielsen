﻿using System;

namespace H5OefeningenDeel3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H5-schaar-steen-papier");

            const byte MaxGames = 3;

            int scorePlayer = 0;
            int scoreComputer = 0;

            while (scorePlayer < MaxGames && scoreComputer < MaxGames)

                Console.WriteLine("Kies PAPIER (1), SCHAAR (2), STEEN (3) : ");
            int inputPlayer = Convert.ToInt32(Console.ReadLine());

            Random random = new Random();
            int inputComputer = random.Next(1, 4);
            Console.WriteLine($"Jij hebt {inputPlayer}, computer {inputComputer}");

            if (inputComputer != inputPlayer)
            {
                if (inputComputer == 3 && inputPlayer == 1)
                {
                    scorePlayer++;
                }
                else if (inputComputer == 1 && inputPlayer == 2)
                {
                    scorePlayer++;
                }
                else if (inputComputer == 2 && inputPlayer == 3)
                {
                    scorePlayer++;
                }
                else if (inputComputer == 3 && inputPlayer == 2)
                {
                    scoreComputer++;
                }
                else if (inputComputer == 1 && inputPlayer == 3)
                {
                    scoreComputer++;
                }

            }



        }
    }
}
