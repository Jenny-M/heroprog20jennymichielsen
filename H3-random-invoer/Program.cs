﻿using System;

namespace H3_random_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randomGen = new Random();
            Console.WriteLine("Voer bedrag in?");
            int sum = 0;
            sum += randomGen.Next(1, 51);
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += randomGen.Next(1, 51);
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += randomGen.Next(1, 51);
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += randomGen.Next(1, 51);
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("Voer bedrag in?");
            sum += randomGen.Next(1, 51);
            Console.WriteLine($"De poef staat op {sum} euro");
            Console.WriteLine("****************************");
            Console.WriteLine($"Het totaal van de poef is {sum} euro.");
            Console.WriteLine($"Dit zal {Math.Ceiling(sum / 10.0)} afbetalingen vragen.");
        }
    }
}
