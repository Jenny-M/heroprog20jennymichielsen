﻿using System;

namespace H1_beetje_wiskunde
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Een wannabe rekenmachine");
            Console.WriteLine();
            Console.WriteLine("Berekening met gebruik van integers");
            Console.WriteLine(-1 + 4 * 6);
            Console.WriteLine((35 + 5) % 7);
            Console.WriteLine(14 + -4 * 6 / 11);
            Console.WriteLine(2 + 15 / 6 * 1 - 7 % 2);
            Console.WriteLine();
            Console.WriteLine("Berekening met gebruik van floats");

            Console.WriteLine(-1 + 4.0f * 6);
            Console.WriteLine((35.0f + 5) % 7);
            Console.WriteLine(14 + -4.0f * 6 / 11);
            Console.WriteLine(2 + 15.0f / 6 * 1 - 7.0f % 2);
            Console.ReadLine();
        }
    }
}
