﻿using System;

namespace H3_xor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een eerste getal?");
            int number1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef een tweede getal?");
            int number2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef nummer van de bit (te tellen vanaf de kleinste) ? ");
            int shift = Convert.ToInt32(Console.ReadLine()) - 1;
            Console.WriteLine($"Precies één bit met waarde 1:{Convert.ToBoolean(((number1 >> shift) ^ (number2 >> shift)) & 1)}");
        }
    }
}
