﻿using System;

namespace H1_gemiddelde
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wannabe rekenmachine: gemiddelde");
            Console.WriteLine();
            Console.WriteLine("Berekening gemiddelde met gebruik van integers");
            Console.WriteLine((18 + 11 + 8) / 3);
            Console.WriteLine();
            Console.WriteLine("Berekening gemiddelde met gebruik van floats");
            Console.WriteLine((18.0f + 11 + 8) / 3);
            Console.ReadLine();
        }
    }
}
