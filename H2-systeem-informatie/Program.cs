﻿using System;

namespace H2_systeem_informatie
{
    class Program
    {
        static void Main(string[] args)
        {
            bool is64bit = Environment.Is64BitOperatingSystem; string pcName = Environment.MachineName;
            int procCount = Environment.ProcessorCount; string userName = Environment.UserName;
            long memory = Environment.WorkingSet;
            string currentDirectory = Environment.CurrentDirectory; OperatingSystem osVersion = Environment.OSVersion;
            Console.WriteLine($"Uw computer heeft een 64-bit besturingssysteem: {is64bit}"); Console.WriteLine($"De naam van uw pc is: {pcName}");
            Console.WriteLine($"Uw pc heeft {procCount} processorkernen."); Console.WriteLine($"{userName} is uw gebruikersnaam.");
            Console.WriteLine($"Je gebruikt {memory / 1000000} Mb aan geheugen"); Console.WriteLine($"Je werkt momenteel in de directory {currentDirectory}."); Console.WriteLine($"Jouw werkt met {osVersion}");
            Console.ReadLine();
        }
    }
}
