﻿using System;

namespace H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            double kilometerstandBijAanvang = 0d;
            double kilometerstandBijAankomst = 0d;
            double aantalLiterinTankBijVertrek = 0d;
            double aantalLiterinTankBijAankomst = 0d;
            double verbruik = 0d;

            Console.Write("Geef het aantal liter in tank voor de rit: ");
            aantalLiterinTankBijVertrek = double.Parse(Console.ReadLine());
            Console.Write("Geef het aantal liter in tank na de rit: ");
            aantalLiterinTankBijAankomst = double.Parse(Console.ReadLine());
            Console.Write("Geef kilometerstand van je auto voor de rit: ");
            kilometerstandBijAanvang = double.Parse(Console.ReadLine());
            Console.Write("Geef kilometerstand van je auto na de rit: ");
            kilometerstandBijAankomst = double.Parse(Console.ReadLine());

            verbruik = 100 * (aantalLiterinTankBijVertrek - aantalLiterinTankBijAankomst) / (kilometerstandBijAankomst - kilometerstandBijAanvang);

            Console.WriteLine($"Het verbruik van de auto is: {verbruik}");

            Console.WriteLine($"Het afgeronde verbruik van de auto is: {Math.Round(verbruik, 2)}");

            Console.ReadLine();
        }
    }
}
